﻿using System.Collections.Generic;
using Moodle.Net.Models;
using Newtonsoft.Json;

namespace Moodle.Net.Responses
{
    /// <summary>
    /// Base response JSON from Moodle when getting courses by course ID.
    /// </summary>
    public class GetCourseByCourseIdResponse : BaseMoodleResponse
    {
        /// <summary>
        /// List of courses found.
        /// </summary>
        [JsonProperty("courses")]
        public List<Course> Courses { get; set; }

        /// <summary>
        /// List of any warnings returned.
        /// </summary>
        [JsonProperty("warnings")]
        public List<object> Warnings { get; set; }
    }
}