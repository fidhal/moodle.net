﻿using System;

namespace Moodle.Net.Exceptions
{
    /// <summary>
    /// Exception for when the request has gone wrong for an unknown reason.
    /// </summary>
    public class BadRequestException : Exception
    {
        /// <summary>
        /// The default constructor for the exception.
        /// </summary>
        public BadRequestException()
        {
        }

        /// <summary>
        /// An alternative constructor for the exception if you specify a exception message.
        /// </summary>
        /// <param name="message">Exception message.</param>
        public BadRequestException(string message) : base(message)
        {
        }
    }
}