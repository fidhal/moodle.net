﻿using System;

namespace Moodle.Net.Exceptions
{
    /// <summary>
    /// Exception for when there is a missing parameter in a request.
    /// </summary>
    public class InvalidParameterException : Exception
    {
        /// <summary>
        /// The default constructor for the exception.
        /// </summary>
        public InvalidParameterException()
        {
        }

        /// <summary>
        /// An alternative constructor for the exception if you specify a exception message.
        /// </summary>
        /// <param name="message">Exception message.</param>
        public InvalidParameterException(string message) : base(message)
        {
        }
    }
}