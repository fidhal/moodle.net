﻿using System;

namespace Moodle.Net.Exceptions
{
    /// <summary>
    /// Exception for when the WsToken is invalid.
    /// </summary>
    public class InvalidTokenException : Exception
    {
        /// <summary>
        /// The default constructor for the exception.
        /// </summary>
        public InvalidTokenException()
        {
        }

        /// <summary>
        /// An alternative constructor for the exception if you specify a exception message.
        /// </summary>
        /// <param name="message">Exception message.</param>
        public InvalidTokenException(string message) : base(message)
        {
        }
    }
}