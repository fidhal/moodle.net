﻿using System;

namespace Moodle.Net.Exceptions
{
    /// <summary>
    /// Exception for when the login request fails.
    /// </summary>
    public class LoginFailedException : Exception
    {
        /// <summary>
        /// The default constructor for the exception.
        /// </summary>
        public LoginFailedException()
        {
        }

        /// <summary>
        /// An alternative constructor for the exception if you specify a exception message.
        /// </summary>
        /// <param name="message">Exception message.</param>
        public LoginFailedException(string message) : base(message)
        {
        }
    }
}