﻿using Newtonsoft.Json;

namespace Moodle.Net.Models
{
    /// <summary>
    /// Base response JSON from Moodle if exception is thrown.
    /// </summary>
    public class BaseMoodleResponse
    {
        /// <summary>
        /// The request's exception.
        /// </summary>
        [JsonProperty("exception", NullValueHandling = NullValueHandling.Ignore)]
        public string Exception { get; set; }

        /// <summary>
        /// The request's error code.
        /// </summary>
        [JsonProperty("errorcode", NullValueHandling = NullValueHandling.Ignore)]
        public string ErrorCode { get; set; }

        /// <summary>
        /// The request's error message.
        /// </summary>
        [JsonProperty("message", NullValueHandling = NullValueHandling.Ignore)]
        public string Message { get; set; }

        /// <summary>
        /// The information on the requests missing values.
        /// </summary>
        [JsonProperty("debuginfo", NullValueHandling = NullValueHandling.Ignore)]
        public string DebugInfo { get; set; }
    }
}