﻿using System;
using Newtonsoft.Json;

namespace Moodle.Net.Models
{
    /// <summary>
    /// An object used to represent a Moodle User.
    /// </summary>
    public class User : BaseMoodleResponse
    {
        /// <summary>
        /// The user's Moodle username.
        /// </summary>
        [JsonProperty("username")]
        public string Username { get; set; }
        
        /// <summary>
        /// The user's first name.
        /// </summary>
        [JsonProperty("firstname")]
        public string Firstname { get; set; }

        /// <summary>
        /// The user's last name.
        /// </summary>
        [JsonProperty("lastname")]
        public string Lastname { get; set; }

        /// <summary>
        /// The user's full name.
        /// </summary>
        [JsonProperty("fullname")]
        public string Fullname { get; set; }
        
        /// <summary>
        /// The user's unique ID.
        /// </summary>
        [JsonProperty("userid")]
        public long UserId { get; set; }
 
        /// <summary>
        /// The user's profile picture URL.
        /// </summary>
        [JsonProperty("userpictureurl")]
        public Uri UserPictureUrl { get; set; }
        
        /// <summary>
        /// The user's Moodle's site name.
        /// </summary>
        [JsonProperty("sitename")]
        public string SiteName { get; set; }
    }
}