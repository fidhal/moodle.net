﻿using Newtonsoft.Json;

namespace Moodle.Net.Models
{
    /// <summary>
    /// An object used to represent a Moodle Course.
    /// </summary>
    public class Course : BaseMoodleResponse
    {
        /// <summary>
        /// The course's unique Moodle ID.
        /// </summary>
        [JsonProperty("id")]
        public int Id { get; set; }

        /// <summary>
        /// The course's short name.
        /// </summary>
        [JsonProperty("shortname")]
        public string Shortname { get; set; }

        /// <summary>
        /// The course's full name.
        /// </summary>
        [JsonProperty("fullname")]
        public string Fullname { get; set; }

        /// <summary>
        /// The course's display name.
        /// </summary>
        [JsonProperty("displayname")]
        public string Displayname { get; set; }

        /// <summary>
        /// The course's summary.
        /// </summary>
        [JsonProperty("summary")]
        public string Summary { get; set; }
    }
}