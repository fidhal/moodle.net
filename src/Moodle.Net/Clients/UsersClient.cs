﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using HtmlAgilityPack;
using Moodle.Net.Exceptions;
using Moodle.Net.Models;

namespace Moodle.Net.Clients
{
    /// <summary>
    /// A class to provide methods to interact with the <code>User</code> section of the Moodle API.
    /// </summary>
    public class UsersClient : BaseClient
    {
        private readonly MoodleClient _moodleClient;

        internal UsersClient(MoodleClient moodleClient)
        {
            _moodleClient = moodleClient;
        }
        
        internal UsersClient(MoodleClient moodleClient, string wsToken) : base(wsToken)
        {
            _moodleClient = moodleClient;
        }
        
        /// <summary>
        /// Gets the current Moodle user if logged in.
        /// </summary>
        /// <returns>A <see cref="User"/> object for the logged in user.</returns>
        public async Task<User> GetCurrentUserAsync()
        {
            var user = await SendAuthenticatedRequestAsync<User>(function: "core_webservice_get_site_info");
            
            if(user.Exception != null)
                if (user.ErrorCode == "invalidtoken")
                    throw new InvalidTokenException();
                else
                    throw new BadRequestException();
                
            _moodleClient.LoggedInUser = user;
			
            return user;
        }

        /// <summary>
        /// Login to Moodle using an username and password.
        /// </summary>
        /// <param name="username">The user's username.</param>
        /// <param name="password">The user's password.</param>
        /// <exception cref="LoginFailedException">Thrown when the login process fails.</exception>
        public async Task LoginAsync(string username, string password)
        {
            var loginPageResponse = await SendUnauthenticatedRequestAsync<HttpResponseMessage>(
                new Uri(Constants.BaseUri, "login/index.php"), HttpMethod.Get, returnJson: false);
            
            var loginPageDocument = new HtmlDocument();
            loginPageDocument.LoadHtml(await loginPageResponse.Content.ReadAsStringAsync());
            
            var loginTokenHtml = loginPageDocument.QuerySelector("input[name='logintoken']").Attributes.FirstOrDefault(a => a.Name == "value");

            if (loginTokenHtml == null)
                throw new LoginFailedException();

            var loginSubmitResponse = await SendUnauthenticatedRequestAsync<HttpResponseMessage>(new Uri(Constants.BaseUri, "login/index.php"), 
                HttpMethod.Post, new Dictionary<string, string>
                {
                    {"logintoken", loginTokenHtml.Value},
                    {"username", username},
                    {"password", password},
                    {"anchor", ""},
                },
                false);
            
            var loginSubmitDocument = new HtmlDocument();
            loginSubmitDocument.LoadHtml(await loginSubmitResponse.Content.ReadAsStringAsync());

            if (loginSubmitDocument.QuerySelector("title").InnerText.ToLower().Contains("log in"))
                throw new LoginFailedException();

            var tokenResponse = await SendUnauthenticatedRequestAsync<dynamic>(
                new Uri(Constants.BaseUri, $"/login/token.php?username={username}&password={password}&service=moodle_mobile_app"),
                HttpMethod.Get);

            _moodleClient.LoggedInUserWsToken = tokenResponse.token.ToString();
            _moodleClient.SetWsToken(tokenResponse.token.ToString());

            await GetCurrentUserAsync();
        }

        /// <summary>
        /// Gets the courses the logged in user is enrolled to.
        /// </summary>
        /// <returns>A list of <see cref="Course"/>s.</returns>
        public async Task<List<Course>> GetCoursesForUser()
        {
            var courses = await SendAuthenticatedRequestAsync<List<Course>>(function: "core_enrol_get_users_courses",
                nameValuePairs: new Dictionary<string, string>
                {
                    {"userid", _moodleClient.LoggedInUser.UserId.ToString()}
                });
            
            if(courses.Any(a => a.Exception != null))
                if (courses.Any(a => a.ErrorCode == "invalidparameter"))
                    throw new InvalidParameterException();
                else
                    throw new BadRequestException();

            return courses;
        }
    }
}