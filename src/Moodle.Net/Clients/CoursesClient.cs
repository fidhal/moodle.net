﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Moodle.Net.Models;
using Moodle.Net.Responses;

namespace Moodle.Net.Clients
{
    /// <summary>
    /// A class ot provide methods to interact with the <code>Course</code> section of the Moodle API.
    /// </summary>
    public class CoursesClient : BaseClient
    {
        private readonly MoodleClient _moodleClient;

        internal CoursesClient(MoodleClient moodleClient)
        {
            _moodleClient = moodleClient;
        }
        
        internal CoursesClient(MoodleClient moodleClient, string wsToken) : base(wsToken)
        {
            _moodleClient = moodleClient;
        }

        /// <summary>
        /// Tries to get a course by course ID.
        /// </summary>
        /// <param name="courseId">The specified course ID.</param>
        /// <returns>A list of <see cref="Course"/>s.</returns>
        public async Task<Course> GetCourseByCourseId(int courseId)
        {
            var response = await SendAuthenticatedRequestAsync<GetCourseByCourseIdResponse>(function: "core_course_get_courses_by_field",
                nameValuePairs: new Dictionary<string, string>
                {
                    {"field", "id"},
                    {"value", courseId.ToString()}
                });

            return response.Courses.Count == 0 ? null : response.Courses.First();
        }
    }
}