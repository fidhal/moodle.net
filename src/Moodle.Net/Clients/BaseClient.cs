﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Moodle.Net.Models;
using Newtonsoft.Json;

namespace Moodle.Net.Clients
{
    /// <summary>
    /// The base class for a sub-client. Contains the methods required to interact with the Moodle API.
    /// </summary>
    public abstract class BaseClient
    {
        private readonly HttpClient _httpClient;
        internal string WsToken { get; private set; }
        internal static CookieContainer CookieContainer => new CookieContainer();
        
        internal BaseClient()
        {
            _httpClient = new HttpClient(new HttpClientHandler
            {
                CookieContainer = new CookieContainer(),
            });
        }

        internal BaseClient(string wsToken)
        {
            _httpClient = new HttpClient(new HttpClientHandler
            {
                CookieContainer = CookieContainer,
            });
            WsToken = wsToken;
        }

        internal void SetWsToken(string wsToken)
        {
            WsToken = wsToken;
        }

        internal async Task<T> SendUnauthenticatedRequestAsync<T>(Uri requestUri, HttpMethod httpMethod, Dictionary<string, string> nameValuePairs = null,
            bool returnJson = true)
        {
            return await SendRequestAsync<T>(requestUri, httpMethod, nameValuePairs, returnJson: returnJson);
        }
        
        internal async Task<T> SendAuthenticatedRequestAsync<T>(Uri requestUri = null, HttpMethod httpMethod = null, Dictionary<string, string> nameValuePairs = null,
            string function = null, bool returnJson = true
            )
        {
            return await SendRequestAsync<T>(requestUri, httpMethod, nameValuePairs, function,true, returnJson);
        }

        private async Task<T> SendRequestAsync<T>(Uri requestUri = null, HttpMethod httpMethod = null, Dictionary<string, string> nameValuePairs = null, string function = null,
            bool authenticated = false, bool returnJson = true)
        {
            if (requestUri == null)
                requestUri = new Uri(Constants.BaseUri, "webservice/rest/server.php?moodlewsrestformat=json");
            
            if(httpMethod == null)
                httpMethod = HttpMethod.Post;
            
            nameValuePairs ??= new Dictionary<string, string>();
            HttpContent content = null;
            
            if (authenticated)
                nameValuePairs.Add("wstoken", WsToken);
            
            if(!string.IsNullOrEmpty(function))
                nameValuePairs.Add("wsfunction", function);

            if(nameValuePairs.Count != 0)
                content = new FormUrlEncodedContent(nameValuePairs);
            
            var request = new HttpRequestMessage
            {
                RequestUri = requestUri,
                Method = httpMethod,
                Content = content,
                Headers =
                {
                    {"user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.135 Safari/537.36"}
                }
            };
            
            if(!authenticated)
                request.Headers.Add("upgrade-insecure-requests", "1");
            
            var response = await _httpClient.SendAsync(request);
            if (returnJson)
                return JsonConvert.DeserializeObject<T>(await response.Content.ReadAsStringAsync());
            else
                return (T)Convert.ChangeType(response, typeof(T));
        }
    }
}