﻿using Moodle.Net.Models;

namespace Moodle.Net.Clients
{
    /// <summary>
    /// The main class of the library. Allows access to each of the sub-clients.
    /// </summary>
    public class MoodleClient
    {
        /// <summary>
        /// The User sub-client.
        /// </summary>
        public UsersClient Users { get; }
        
        /// <summary>
        /// The Courses sub-client.
        /// </summary>
        public CoursesClient Courses { get; }

        /// <summary>
        /// The currently logged-in user.
        /// </summary>
        public User LoggedInUser { get; set; }

        /// <summary>
        /// The currently logged-in user's <c>wsToken</c>.
        /// </summary>
        public string LoggedInUserWsToken { get; set; }

        /// <summary>
        /// The default constructor for the Moodle client.
        /// </summary>
        public MoodleClient()
        {
            Users = new UsersClient(this);
            Courses = new CoursesClient(this);
        }

        /// <summary>
        /// An alternative constructor which takes a <c>wsToken</c> on initialisation.
        /// </summary>
        /// <param name="wsToken">The WsToken to use</param>
        public MoodleClient(string wsToken)
        {
            Users = new UsersClient(this, wsToken);
            Courses = new CoursesClient(this, wsToken);

            LoggedInUser = Users.GetCurrentUserAsync().GetAwaiter().GetResult();
            LoggedInUserWsToken = wsToken;
        }

        /// <summary>
        /// Sets the <c>wsToken</c> to be used across all the sub-clients.
        /// </summary>
        /// <param name="wsToken">The WsToken to use</param>
        public void SetWsToken(string wsToken)
        {
            Users.SetWsToken(wsToken);
            
            LoggedInUserWsToken = wsToken;
        }
    }
}